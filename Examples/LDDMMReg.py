#
# LDDMM relaxation-based registration, not fully tested
#

from PyCA.Core import *

import PyCA.Common as common
import PyCA.Display as display

import numpy as np
import matplotlib.pyplot as plt

import os, errno

def LDDMMReg(I0Orig,
             I1Orig, 
             scales = [1], 
             nIters = [1000], 
             maxPert = [0.2],
             smoothC = [1.0],
             nTimesteps = 10,
             fluidParams = [0.1, 0.1, 0.001], 
             plotEvery = 100):

    mType = I0Orig.memType()
    origGrid = I0Orig.grid()

    # allocate vars
    I0 = Image3D(origGrid, mType)
    I1 = Image3D(origGrid, mType)
    h = Field3D(origGrid, mType)
    diff = Image3D(origGrid, mType)
    gI = Field3D(origGrid, mType)
    gV = Field3D(origGrid, mType)
    scratchI = Image3D(origGrid, mType)
    scratchV = Field3D(origGrid, mType)
    # deformed images
    J = [Image3D(origGrid, mType) for _ in range(nTimesteps)]
    # vector fields
    v = [Field3D(origGrid, mType) for _ in range(nTimesteps)]
    for t in range(nTimesteps):
        SetMem(v[t], 0.0)

    # allocate diffOp
    if mType == MEM_HOST:
        diffOp = FluidKernelFFTCPU()
    else:
        diffOp = FluidKernelFFTGPU()

    # initialize some vars
    zerovec = Vec3Df(0.0, 0.0, 0.0)

    nScales = len(scales)
    scaleManager = MultiscaleManager(origGrid)
    for s in scales:
        scaleManager.addScaleLevel(s)

    # Initalize the thread memory manager (needed for resampler)
    # num pools is 2 (images) + 2*3 (fields)
    ThreadMemoryManager.init(origGrid, mType, 8)

    if mType == MEM_HOST:
        resampler = MultiscaleResamplerGaussCPU(origGrid)
    else:
        resampler = MultiscaleResamplerGaussGPU(origGrid)

    def setScale(scale):
        global curGrid

        scaleManager.set(scale)
        curGrid = scaleManager.getCurGrid()
        # since this is only 2D:
        curGrid.spacing().z = 1.0;

        resampler.setScaleLevel(scaleManager)

        diffOp.setAlpha(fluidParams[0])
        diffOp.setBeta(fluidParams[1])
        diffOp.setGamma(fluidParams[2])
        diffOp.setGrid(curGrid)

        # downsample images
        I0.setGrid(curGrid)
        I1.setGrid(curGrid)
        if scaleManager.isLastScale():
            Copy(I0, I0Orig)
            Copy(I1, I1Orig)
        else:
            resampler.downsampleImage(I0,I0Orig)
            resampler.downsampleImage(I1,I1Orig)

        # initialize / upsample deformation
        if scaleManager.isFirstScale():
            h.setGrid(curGrid)
            SetToIdentity(h)
        else:
            resampler.updateHField(h)

        # set grids
        gI.setGrid(curGrid)
        diff.setGrid(curGrid)
        gV.setGrid(curGrid)
        scratchI.setGrid(curGrid)
        scratchV.setGrid(curGrid)
        for t in range(nTimesteps):
            J[t].setGrid(curGrid)
            v[t].setGrid(curGrid)
    # end function

    energy = [[] for _ in xrange(3)]

    ustep = None

    for scale in range(len(scales)):

        setScale(scale)
        ustep = None

        for it in range(nIters[scale]):
            print 'iter %d'%it

            # compute deformed images
            SetToIdentity(h)
            for t in range(nTimesteps):
                ComposeHVInv(scratchV, h, v[t])
                h.swap(scratchV)
                ApplyH(J[t], I0, h)

            # difference image
            Sub(diff, J[-1], I1)
            
            # compute deformed images
            SetToIdentity(h)
            for t in range(nTimesteps-1,-1,-1):
                ComposeVInvH(scratchV, v[t], h)
                h.swap(scratchV)
                Gradient(gI, J[t])
                Splat(scratchI, h, diff)
                Mul_I(gI, scratchI)
                diffOp.applyInverseOperator(gV, gI)
                MulC_I(gV, smoothC[scale])
                # check maxpert
                Magnitude(scratchI, gV)
                gradmax = Max(scratchI)
                if ustep is None or ustep*gradmax > maxPert:
                    ustep = maxPert[scale]/gradmax
                    print 'step is %f'%ustep
                # u =  u*(1-2.0*ustep) + (2.0*ustep)*gU
                MulC_Add_MulC_I(v[t], (1-2.0*ustep),
                                gV, 2.0*ustep)
            
            gV *= ustep
            # ApplyV(scratchV, h, gV, BACKGROUND_STRATEGY_PARTIAL_ID)
            ComposeHV(scratchV, h, gV)
            h.swap(scratchV)

            # compute energy
            energy[0].append(Sum2(diff))

            if it == nIters[scale]-1 or \
                   (plotEvery is not None and it % plotEvery == 0) :
                clrlist = ['r','g','b','m','c','y','k']
                plt.figure('energy')
                for i in range(len(energy)):
                    plt.plot(energy[i],clrlist[i])
                    if i == 0:
                        plt.hold(True)
                plt.hold(False)
                plt.draw()

                plt.figure('results')
                plt.clf()
                plt.subplot(3,2,1)
                display.DispImage(I0, 'I0', newFig=False)
                plt.subplot(3,2,2)
                display.DispImage(I1, 'I1', newFig=False)
                plt.subplot(3,2,3)
                display.DispImage(J[-1], 'def (I0 -> I1)', newFig=False)
                plt.subplot(3,2,4)
                display.DispImage(diff, 'diff', newFig=False)
                plt.colorbar()
                # plt.subplot(3,2,5)
                # display.GridPlot(h, every=4, isVF=False)
                # plt.draw()
                plt.show()

            # end plot
        # end iteration
    # end scale
    return (J[-1].copy(), h, energy)
# end function

if __name__ == '__main__':

    plt.close('all')

    if GetNumberOfCUDADevices() > 0:
        mType = MEM_DEVICE
    else:
        print "No CUDA devices found, running on CPU"
        mType = MEM_HOST

    imagedir='./Images/'

    #
    # Run lena images
    #

    I0 = common.LoadPNGImage(imagedir + 'lena_deformed.png', mType)
    I1 = common.LoadPNGImage(imagedir + 'lena_orig.png', mType)

    (Idef, h, energy) = \
           LDDMMReg(I0, 
                    I1, 
                    scales = [2,1], 
                    nIters = [300,300], 
                    maxPert = [0.05, 0.05],
                    smoothC = [2.0, 2.0],
                    nTimesteps = 10,
                    fluidParams = [0.5, 0.5, 0.001], 
                    plotEvery = 500)

