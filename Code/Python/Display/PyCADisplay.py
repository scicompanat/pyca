import PyCA.Core as ca
import PyCA.Common as common
import PyCA.Numpy as canp

import numpy as np
import matplotlib.cm as cm

# pyplot contains most of the plotting functionality
try:
    import matplotlib.pyplot as plt
    import matplotlib.colors
    plt_loaded = True
    # tell it to use interactive mode -- see results immediately
    plt.ion()
except:
    print "Warning: matplotlib import failed, some functionality disabled"


def EnergyPlot(energy, trim=0.05, legend=None):
    # trim
    ttlEn = np.array(energy[-1])
    nEl = len(ttlEn)
    ascendingEn = sorted(ttlEn)
    nToTrim = int(nEl*trim)
    trimVal = ascendingEn[-(1+nToTrim)]
    en = np.array(energy)
    en[en > trimVal] = float('NaN')
    # plot
    colorList = ['r', 'g', 'b', 'k', 'm', 'c', 'y']
    for idx in range(len(en)):
        plt.plot(en[idx][:], colorList[idx])
        if idx == 0:
            plt.hold(True)
    plt.hold(False)
    if legend is not None:
        plt.legend(legend)
    plt.draw()


def _world_points_offsets(vf, isVF=True):
    """
    Generate numpy arrays of world-space grid and offsets for given
    vector field
    """

    offsets = common.AsNPCopy(vf)
    if not isVF:
        offsets = canp.HtoV(offsets, sp=vf.spacing())

    sz = np.array(vf.size().tolist())
    sp = np.array(vf.spacing().tolist())
    points = canp.identity(sz)*sp

    return points, offsets

def _world_slice_points_offsets(vf, sliceIdx=None, dim='z',
                                isVF=True, every=1):
    """
    Generate numpy arrays of world-space grid and offsets for
    specified slice of given vector field
    """


    sliceVF = common.ExtractSliceVF(vf, sliceIdx, dim)

    points, offsets = _world_points_offsets(vf=sliceVF, isVF=isVF)

    dimidx = common.DIMMAP[dim]
    points = np.delete(points, dimidx, axis=-1)
    offsets = np.delete(offsets, dimidx, axis=-1)

    points = np.squeeze(points)[::every, ::every]
    offsets = np.squeeze(offsets)[::every, ::every]

    return points, offsets


def Quiver(vf, sliceIdx=None, dim='z', isVF=True, every=1, color='g'):

    points, offsets = \
        _world_slice_points_offsets(vf=vf, sliceIdx=sliceIdx, dim=dim,
                                    isVF=isVF, every=every)

    plt.quiver(points[..., 0], points[..., 1],
               offsets[..., 0], offsets[..., 1],
               units='xy',
               scale=1, color=color)


def GridPlot(vf, sliceIdx=None, dim='z', every=1,
             isVF=True, color='g', plotBase=True, colorbase='#A0A0FF'):
    if plotBase:
        Id = vf.copy()
        ca.SetToIdentity(Id)
        GridPlot(Id, sliceIdx=sliceIdx, dim=dim, every=every,
                 isVF=False, color=colorbase, plotBase=False)
        plt.hold(True)

    points, offsets = \
        _world_slice_points_offsets(vf=vf, sliceIdx=sliceIdx, dim=dim,
                                    isVF=isVF, every=every)

    xpts = points[..., 0] + offsets[..., 0]
    ypts = points[..., 1] + offsets[..., 1]
    plt.plot(xpts, ypts, color)
    plt.hold(True)
    plt.plot(xpts.T, ypts.T, color)
    plt.hold(False)
    # change axes to match image axes
    # if not plt.gca().yaxis_inverted():
    #     plt.gca().invert_yaxis()
    #     # force redraw
    #     plt.draw()

def JacDetCMap(jd_max=10.0, cmap='PRGn',
               nonpos_clr=(1.0, 0.0, 0.0, 1.0)):
    """
    Create a colormap for displaying jacobian determinant that clamps
    over and under values to colormap boundary colors, but sets 'bad'
    values to nonpos_clr since logmapped zero or negative numbers will
    be -inf or nan
    """
    jacdet_cmap = plt.cm.__dict__[cmap]
    jacdet_cmap.set_over(color=jacdet_cmap(1.0))
    jacdet_cmap.set_under(color=jacdet_cmap(0.0))
    jacdet_cmap.set_bad(color=nonpos_clr)
    return jacdet_cmap

def JacDetPlot(vf, title='Jac. Det.',
               jd_max=10.0, cmap='PRGn',
               nonpos_clr=(1.0, 0.0, 0.0, 1.0),
               sliceIdx=None, dim='z',
               isVF=True,
               newFig=False):
    """
    Plot the jacobian determinant using logmapped colors, and setting
    zero or negative values to 'nonpos_clr'.  jd_max is the maximum
    value to display without clamping, and also defines the min value
    as 1.0/jd_max to assure 1.0 is centered in the colormap.  If vf is
    a vector field, compute the jacobian determinant.  If it is an
    Image3D, assume it is the jacobian determinant to be plotted.
    """

    if common.IsField3D(vf):
        grid = vf.grid()
        mType = vf.memType()
        h = ca.ManagedField3D(grid, mType)
        jacdet = ca.ManagedImage3D(grid, mType)
        ca.Copy(h, vf)
        if isVF:
            ca.VtoH_I(h)
        ca.JacDetH(jacdet, h)
    elif common.IsImage3D(vf):
        jacdet = vf
    else:
        raise Exception('unknown input type to JacDetPlot, %s'%\
                        str(type(vf)))
    jd_cmap = JacDetCMap(jd_max=jd_max, cmap=cmap,
                               nonpos_clr=nonpos_clr)
    DispImage(jacdet, title=title,
              sliceIdx=sliceIdx, dim=dim,
              rng=[1.0/jd_max, jd_max],
              cmap=jd_cmap,
              log=True,
              newFig=newFig)

def SaveSlice(fname, im, sliceIdx=None, dim='z',
              cmap='gray', rng=None, t=True):
    sliceIm = common.ExtractSliceIm(im, sliceIdx, dim)
    sliceIm.toType(ca.MEM_HOST)
    if rng is None:
        vmin = None
        vmax = None
    else:
        vmin = rng[0]
        vmax = rng[1]
    if t:
        plt.imsave(fname, np.squeeze(sliceIm.asnp()).T,
                   cmap=cmap, vmin=vmin, vmax=vmax)
    else:
        plt.imsave(fname, np.squeeze(sliceIm.asnp()),
                   cmap=cmap, vmin=vmin, vmax=vmax)


def SaveThreePaneArr(npim, outname, cmap='gray', rng=None):

    sz = npim.shape
    xSlice = np.squeeze(npim[sz[0]/2, :, :])
    ySlice = np.squeeze(npim[:, sz[1]/2, :])
    zSlice = np.squeeze(npim[:, :, sz[2]/2])
    sumx = xSlice.shape[1] + ySlice.shape[1] + zSlice.shape[1]
    maxy = max([xSlice.shape[0], ySlice.shape[0], zSlice.shape[0]])
    threePane = np.zeros([maxy, sumx])
    starty = (maxy-xSlice.shape[0])/2
    threePane[starty:starty+xSlice.shape[0], 0:xSlice.shape[1]] = xSlice
    startx = xSlice.shape[1]
    starty = (maxy-ySlice.shape[0])/2
    threePane[starty:starty+ySlice.shape[0],
              startx:startx+ySlice.shape[1]] = ySlice
    startx = startx+ySlice.shape[1]
    starty = (maxy-zSlice.shape[0])/2
    threePane[starty:starty+zSlice.shape[0],
              startx:startx+zSlice.shape[1]] = zSlice

    if rng is None:
        vmin = None
        vmax = None
    else:
        vmin = rng[0]
        vmax = rng[1]

    plt.imsave(outname,
               threePane, cmap=cmap,
               vmin=vmin, vmax=vmax)


#
# Takes an array of Field3Ds, an Image3D, and a series of
# timepoints, and a function and optional args.  Calls function as:
# func(Idef, t, *args)
# for deformed image at each timepoint, and appends the result to a
# list that is returned from this function
#
def DefSeriesIter(I, V, t, func, args):
    grid = I.grid()
    mType = I.memType()
    tlen = len(t)
    h = ca.Field3D(grid, mType)
    IDef = ca.Image3D(grid, mType)
    ca.SetToIdentity(h)
    scratchV1 = ca.Field3D(grid, mType)
    scratchV2 = ca.Field3D(grid, mType)
    rtnarr = []
    for tidx in range(tlen):
        curt = t[tidx]
        h = common.ComposeDef(V, curt, inverse=True,
                              asVField=False,
                              scratchV1=scratchV1,
                              scratchV2=scratchV2)
        ca.ApplyH(IDef, I, h)
        r = func(IDef, curt, *args)
        rtnarr.append(r)
    return rtnarr


#
# Takes an array of Field3Ds, an Image3D, and a series of
# timepoints and returns a volume containing the frames at these times
#
def ExtractSliceFunc(IDef, t):
    sliceIm = common.ExtractSliceIm(IDef)
    return sliceIm


def GenDefFrames(I, V, t):
    """
    Take an image, a series of velocity fields, and a set of
    timepoints, and return an image that is a stack of slices of I
    deformed to each of the timepoints t.
    """
    grid = I.grid()
    mType = I.memType()

    sliceArr = \
        DefSeriesIter(I, V, t, func=ExtractSliceFunc, args=())

    nSlice = len(sliceArr)

    outGrid = grid.copy()
    outGrid.size().z = nSlice
    sliceStack = ca.Image3D(outGrid, mType)
    start = ca.Vec3Di(0,0,0)

    for sIdx in range(nSlice):
        start.z = sIdx
        ca.SetSubVol_I(sliceStack, sliceArr[sIdx], start)
    return sliceStack


# display an Image3D slice
def DispImage(im, title=None,
              sliceIdx=None, dim='z',
              cmap='gray', newFig=True,
              rng=None, t=True, log=False,
              useOrigin=False, ticks_off=True):

    # extract slice as numpy array
    arr = np.squeeze(common.ExtractSliceArrIm(im, sliceIdx, dim))

    sz2D = im.size().tolist()
    sz2D.pop(common.DIMMAP[dim])
    sp2D = im.spacing().tolist()
    sp2D.pop(common.DIMMAP[dim])
    or2D = im.origin().tolist()
    or2D.pop(common.DIMMAP[dim])

    # create the figure if requested
    if newFig:
        if title is None:
            plt.figure()
        else:
            plt.figure(title)
        plt.clf()

    # set the range
    if rng is None:
        vmin = None
        vmax = None
    else:
        vmin = rng[0]
        vmax = rng[1]

    if log:
        norm = matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax)
    else:
        norm = matplotlib.colors.Normalize(vmin=vmin, vmax=vmax)

    print 'sz2D: ', sz2D, ', sp2D: ', sp2D

    # use spacing and center pixels on indices
    extent = [0.0, sz2D[0]*sp2D[0], 0.0, sz2D[1]*sp2D[1]]
    extent[0] -= sp2D[0]/2.0
    extent[1] -= sp2D[0]/2.0
    extent[2] -= sp2D[1]/2.0
    extent[3] -= sp2D[1]/2.0
    if useOrigin:
        extent[0] += or2D[0]
        extent[1] += or2D[0]
        extent[2] += or2D[1]
        extent[3] += or2D[1]

    # transpose if requested
    if t:
        arr = arr.T
    else:
        extent = [extent[idx] for idx in (2, 3, 0, 1)]

    # display
    plt.imshow(arr, cmap=cmap, vmin=vmin, vmax=vmax, norm=norm,
               interpolation='nearest',
               origin='lower',
               aspect='auto', extent=extent)
    plt.axis('tight')
    plt.axis('image')
    if title is not None:
        plt.title(title)
    if ticks_off:
        plt.xticks([])
        plt.yticks([])
    plt.draw()

def hsv_to_rgb(h, s, v):
    """
    Convert HSV values to RGB values.  Code adapted from python's
    colorsys module (original scalar code commented out)
    """
    # if s == 0.0:
    #     return v, v, v
    # i = int(h*6.0) # XXX assume int() truncates!
    i = (h*6.0).astype(int)  # XXX assume int() truncates!
    f = (h*6.0) - i
    p = v*(1.0 - s)
    q = v*(1.0 - s*f)
    t = v*(1.0 - s*(1.0-f))
    i = i%6
    r = np.zeros(h.shape)
    g = np.zeros(h.shape)
    b = np.zeros(h.shape)

    # if i == 0:
    #     return v, t, p
    mask = i == 0
    r[mask] = v[mask]
    g[mask] = t[mask]
    b[mask] = p[mask]
    # if i == 1:
    #     return q, v, p
    mask = i == 1
    r[mask] = q[mask]
    g[mask] = v[mask]
    b[mask] = p[mask]
    # if i == 2:
    #     return p, v, t
    mask = i == 2
    r[mask] = p[mask]
    g[mask] = v[mask]
    b[mask] = t[mask]
    # if i == 3:
    #     return p, q, v
    mask = i == 3
    r[mask] = p[mask]
    g[mask] = q[mask]
    b[mask] = v[mask]
    # if i == 4:
    #     return t, p, v
    mask = i == 4
    r[mask] = t[mask]
    g[mask] = p[mask]
    b[mask] = v[mask]
    # if i == 5:
    #     return v, p, q
    mask = i == 5
    r[mask] = v[mask]
    g[mask] = p[mask]
    b[mask] = q[mask]

    return r, g, b


def color_2d_vec(v, isVF=False, maxlen=None):
    """
    turn a 2D vector field into an RGB image where vector angle is
    mapped to hue and length is mapped to saturation in HSV space.

    If maxlen is given, this is the vector length at which colors
    reach full saturation.  If not given, the maximum vector length is
    used
    """
    if isinstance(v, ca.Field3D):
        v = common.AsNPCopy(v)
    if not isVF:
        v = canp.HtoV(v)
    v = canp.squeezeField(v)
    sz = v.shape[:2]
    phi = np.arctan2(v[:, :, 0], v[:, :, 1])
    phi = (phi/(2.0*np.pi)) + 0.5
    r = np.sqrt(np.sum(v**2, axis=2))
    if maxlen is None:
        maxlen = np.max(r)
    r /= maxlen
    r = np.minimum(r, 1.0)
    rgb = hsv_to_rgb(phi, r, np.ones(sz))
    return np.stack(rgb, axis=-1)


def SampleHSV(nSamples, saturation=1.0, value=1.0):
    """
    Evenly sample HSV space.  Used to create easy index-to-color
    mappings.
    """
    h = np.arange(nSamples)/float(nSamples)
    s = saturation*np.ones(nSamples)
    v = value*np.ones(nSamples)
    colors = zip(*hsv_to_rgb(h=h, s=s, v=v))

    return colors


def SampleCMap(nSamples, cmap=matplotlib.cm.jet):
    """
    Evenly sample a colormap.  Used to create easy index-to-color
    mappings.
    """
    norm = matplotlib.colors.Normalize(vmin=0, vmax=nSamples)
    sm = matplotlib.cm.ScalarMappable(norm=norm, cmap=cmap)
    colors = sm.to_rgba(np.arange(nSamples))
    return colors


def map_through_cmap(arr, cmap, vmin=None, vmax=None, lognorm=False):
    """
    map the given 2D numpy array 'arr' through colormap 'cmap' (either
    string giving matplotlib colormap name or a colormap instance),
    returning the numpy array representing the color image.
    """
    if lognorm:
        norm = matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax)
    else:
        norm = matplotlib.colors.Normalize(vmin=vmin, vmax=vmax)
    sm = cm.ScalarMappable(norm=norm, cmap=cmap)
    colorarr = sm.to_rgba(arr)
    return colorarr


def map_jacdet_colors(jdarr, jdmax=10.0):
    """
    map the scalar 2D 'jdarr' array through the JacDetCMap colormap,
    returning the numpy array representing the colored image
    """
    rng = [1.0/jdmax, jdmax]
    cmap = JacDetCMap(jd_max=jdmax)
    jdcolorarr = map_through_cmap(arr=jdarr, cmap=cmap,
                                  vmin=rng[0], vmax=rng[1],
                                  lognorm=True)
    return jdcolorarr


def resample_slice_iso(slicearr, sp, slicedim='x'):
    """
    Upsample the 2D (or colored 2D) numpy array slicearr such that it
    has isotropic pixels based on the given spacing (sp) and slicedim
    """

    if isinstance(sp, ca.Vec3Df):
        sp = sp.tolist()

    sz = slicearr.shape
    if len(sz) == 3:
        if sz[2] > 4:
            raise TypeError('slicearr should be a slice image')
        else:
            slicelist = []
            for sidx in range(sz[2]):
                slicelist.append(
                    resample_slice_iso(slicearr[:,:,sidx], sp=sp, slicedim=slicedim))
            return np.stack(slicelist, axis=-1)

    sp = list(sp)
    sp.pop(common.DIMMAP[slicedim])
    pix_aspect = sp[0]/sp[1]
    if pix_aspect > 1.0:
        newsz = (sz[0]*pix_aspect, sz[1])
    elif pix_aspect < 1.0:
        newsz = (sz[0], sz[1]/pix_aspect)
    else:
        return slicearr.copy()

    scale = np.array([float(sz[0])/newsz[0], float(sz[1])/newsz[1]])
    harr = canp.identity(newsz)*scale
    newarr = canp.interp_linear(slicearr, harr)

    return newarr


def draw_colormap(cmap, vmin, vmax, lognorm=False, ticks=None,
                  ticklabels=None, labelsize=None,
                  sz=500, bar_ratio=0.1):
    """
    draw colormap as separate figure with given ticks and tick labels.
    For example, jac det colormap:
    draw_colormap(JacDetCMap(jdmax),
                  vmin=1.0/jdmax, vmax=jdmax, lognorm=True,
                  ticks=[1.0/jdmax, 1.0, jdmax],
                  ticklabels=[r"$%d^{%d}$"%(jdmax, k) for k in (-1, 0, 1)])
    """

    if lognorm:
        norm = matplotlib.colors.LogNorm(vmin=vmin, vmax=vmax)
        # get evenly-spaced samples in log space
        data = 10.0**np.log10(np.logspace(np.log10(vmin), np.log10(vmax), 5))
        data = data[:, None]
    else:
        norm = matplotlib.colors.Normalize(vmin=vmin, vmax=vmax)
        data = np.linspace(vmin, vmax, sz)[:, None]

    # data = norm(data).data
    plt.figure('colormap')
    plt.clf()
    plt.imshow(data, cmap=cmap, norm=norm, extent=[0.0, bar_ratio, 1.0, 0.0])
    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False, # labels along the bottom edge are off
        labeltop=False)    # labels along the top edge are off (default)
    plt.tick_params(
        axis='y',          # changes apply to the y-axis
        which='both',      # both major and minor ticks are affected
        left=False,        # left ticks off
        right=True,        # right ticks on
        labelleft=False,   # labels on left side off
        labelright=True,   # labels on right side on
        labelsize=labelsize, # label font size
        direction='out')   # ticks outside plot
    if ticks is None and ticklabels is not None:
        # evenly spaced ticks
        ticks = np.linspace(vmin, vmax, len(ticklabels))
    if ticks is not None:
        normticks = norm(ticks).data
        if ticklabels is not None:
            assert len(normticks) == len(ticklabels)
            plt.yticks(normticks, ticklabels)
        plt.yticks(normticks)
    plt.gca().invert_yaxis()
    plt.draw()
    plt.show()


def savefig_tight(fname, pad_inches=0.0):
    """
    Save matplotlib figure with tight boundaries and transparent
    background
    """
    plt.savefig(fname,
                transparent=True,
                frameon=False,
                bbox_inches='tight',
                pad_inches=pad_inches)
