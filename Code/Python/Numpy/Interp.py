"""
Interpolation/deformation implemented in numpy.  Meant as reference
implementation to test PyCA functions against, not as optimized
implementations.
"""

import numpy as np

import PyCA.Core as ca
from . import CoordUtils as cutils

#
# Numpy interpolation implementation
#


def identity(sz):
    return cutils.gridcoords(sz)


def VtoH(v, sp=(1.0, 1.0, 1.0)):

    if isinstance(sp, ca.Vec3Df):
        sp = np.array(sp.tolist()).astype(float)

    sz = v.shape[:-1]
    ndims = len(sz)
    assert ndims == v.shape[-1]
    sp = sp[:ndims]

    h = v / sp + identity(sz)

    return h


def HtoV(h, sp=(1.0, 1.0, 1.0)):

    if isinstance(sp, ca.Vec3Df):
        sp = np.array(sp.tolist()).astype(float)

    sz = h.shape[:-1]
    ndims = len(sz)
    assert ndims == h.shape[-1]
    sp = sp[:ndims]

    v = (h - identity(sz)) * sp

    return v


def isVectorField(arr):
    """
    Returns true if arr is shape x-by-y-by-2 or x-by-y-by-z-by-3
    """
    sz = arr.shape
    if len(sz) == 3 and sz[2] == 2 or \
       len(sz) == 4 and sz[3] == 3:
        return True
    return False


def isSlice(arr):
    """
    Returns true if arr is 2D array or vector field
    """
    if isVectorField(arr) and len(arr.shape) == 3 or \
       len(arr.shape) == 2:
        return True
    return False


def squeezeField(farr):
    """
    if farr is a 2D field, squeeze it to x-by-y-by-2
    """

    farr = np.squeeze(farr)
    ndims = len(farr.shape)-1
    farr = farr[..., :ndims]

    return farr


def interp_linear(im, harr,
                  bg=ca.BACKGROUND_STRATEGY_CLAMP,
                  bg_val=0.0):

    im = np.squeeze(im)
    harr = np.squeeze(harr)
    hsz = harr.shape
    n_imdims = hsz[-1]
    hsz = hsz[:-1]
    n_hdims = len(hsz)
    imsz = im.shape
    assert n_imdims == len(im.shape)

    assert bg == ca.BACKGROUND_STRATEGY_CLAMP or \
        bg == ca.BACKGROUND_STRATEGY_WRAP or \
        bg == ca.BACKGROUND_STRATEGY_PARTIAL_ZERO or \
        bg == ca.BACKGROUND_STRATEGY_VAL

    if bg == ca.BACKGROUND_STRATEGY_PARTIAL_ZERO:
        bg_val = 0.0

    if bg == ca.BACKGROUND_STRATEGY_PARTIAL_ZERO or \
            bg == ca.BACKGROUND_STRATEGY_VAL:
        if not isinstance(bg_val, np.ndarray):
            bg_val = bg_val * np.ones(hsz)

    # size of array to hold all weights at all pixels
    wsz = tuple(hsz) + tuple([2]*n_imdims)

    def getw(warr, widx):
        """ Get image-sized array from wsz array given weight coord
        """
        return warr[(Ellipsis,)+tuple(widx)]

    def setw(warr, widx, vals):
        """ Get image-sized array from wsz array given weight coord
        """
        warr[(Ellipsis,)+tuple(widx)] = vals

    h = [harr[..., dim] for dim in range(n_imdims)]

    # previous integer indices
    i_p = [np.floor(hdim).astype(np.int) for hdim in h]
    # next integer indices
    i_n = [i_pdim + 1 for i_pdim in i_p]
    # fractional portion
    f = [h[dim] - i_p[dim] for dim in range(n_imdims)]

    # handle out-of-bounds indices
    if bg == ca.BACKGROUND_STRATEGY_CLAMP:
        i_p = [np.clip(i_p[dim], 0, imsz[dim] - 1) for dim in range(n_imdims)]
        i_n = [np.clip(i_n[dim], 0, imsz[dim] - 1) for dim in range(n_imdims)]
    elif bg == ca.BACKGROUND_STRATEGY_WRAP:
        i_p = [i_p[dim] - (i_p[dim] // imsz[dim]) * imsz[dim]
               for dim in range(n_imdims)]
        i_n = [i_n[dim] - (i_n[dim] // imsz[dim]) * imsz[dim]
               for dim in range(n_imdims)]
    elif bg in (ca.BACKGROUND_STRATEGY_VAL,
                ca.BACKGROUND_STRATEGY_PARTIAL_ZERO):

        i_p_oob_mask = [np.logical_or(i_p[dim] < 0, i_p[dim] >= imsz[dim])
                        for dim in range(n_imdims)]
        i_n_oob_mask = [np.logical_or(i_n[dim] < 0, i_n[dim] >= imsz[dim])
                        for dim in range(n_imdims)]
        # assign some legal value to oob indices
        for dim in range(n_imdims):
            i_p[dim][i_p_oob_mask[dim]] = 0
            i_n[dim][i_n_oob_mask[dim]] = 0
    else:
        raise Exception('unknown bg')

    # compute fractional weights
    # 0-previous, 1-next

    w = np.zeros(wsz)
    Ivals = np.zeros(wsz)
    cit = cutils.getcoorditer([2]*n_imdims)
    for cidx in cit:
        # compute weights
        curw = [f[dim] if cidx[dim] == 1 else 1.0-f[dim]
                for dim in range(n_imdims)]
        curw = reduce(np.multiply, curw)
        setw(w, cidx, curw)
        # get values
        curIvals = im[tuple([i_n[dim] if cidx[dim] == 1 else i_p[dim]
                             for dim in range(n_imdims)])]
        setw(Ivals, cidx, curIvals)

    for dim in range(n_imdims):
        assert np.all(0 <= i_p[dim])
        assert np.all(0 <= i_n[dim])
        assert np.all(i_p[dim] < imsz[dim])
        assert np.all(i_n[dim] < imsz[dim])
    cit = cutils.getcoorditer([2]*n_imdims)
    for cidx in cit:
        assert np.all(0.0 <= getw(w, cidx))
        assert np.all(getw(w, cidx) <= 1.0)

    if bg in (ca.BACKGROUND_STRATEGY_VAL,
              ca.BACKGROUND_STRATEGY_PARTIAL_ZERO):

        cit = cutils.getcoorditer([2]*n_imdims)
        for cidx in cit:
            cur_oob = np.zeros(tuple(hsz)+(n_imdims,), dtype=bool)
            for dim in range(n_imdims):
                cur_oob[..., dim] = \
                    i_n_oob_mask[dim] if cidx[dim] == 1 else i_p_oob_mask[dim]
            cur_oob = np.any(cur_oob, axis=-1)

            cIvals = getw(Ivals, cidx)
            cIvals[cur_oob] = bg_val[cur_oob]
            setw(Ivals, cidx, cIvals)

    return np.sum(w*Ivals, axis=tuple(range(n_hdims, n_imdims+n_hdims)))


def interp_linear_field(varr, harr,
                        bg=ca.BACKGROUND_STRATEGY_PARTIAL_ZERO,
                        bg_val=0.0):

    sz = varr.shape[:-1]
    ndims = len(sz)

    if bg == ca.BACKGROUND_STRATEGY_PARTIAL_ID:
        bg_val_list = [harr[..., dim] for dim in range(ndims)]
        bg = ca.BACKGROUND_STRATEGY_VAL
    else:
        bg_val_list = [bg_val]*ndims

    deflist = [interp_linear(varr[..., dim], harr,
                             bg=bg,
                             bg_val=bg_val_list[dim])
               for dim in range(ndims)]

    return np.stack(deflist, axis=-1)


def ApplyH(arr, harr, bg=ca.BACKGROUND_STRATEGY_CLAMP, bg_val=0.0):

    assert isVectorField(harr)
    is_arr_field = isVectorField(arr)
    arr = np.squeeze(arr)

    if is_arr_field:
        sz = np.squeeze(arr).shape[:-1]
        ndims = len(sz)
        arr = arr[..., :ndims]
    else:
        sz = arr.shape
        ndims = len(sz)

    if is_arr_field:
        out = interp_linear_field(
            arr, harr, bg=bg, bg_val=bg_val)
    else:
        out = interp_linear(
            arr, harr, bg=bg, bg_val=bg_val)

    return out


def ApplyV(arr, varr, bg=ca.BACKGROUND_STRATEGY_CLAMP,
           bg_val=0.0, sp=ca.Vec3Df(1.0, 1.0, 1.0)):

    assert isVectorField(varr)
    is_arr_field = isVectorField(arr)
    arr = np.squeeze(arr)

    if is_arr_field:
        sz = np.squeeze(arr).shape[:-1]
        ndims = len(sz)
        arr = arr[..., :ndims]
    else:
        sz = arr.shape
        ndims = len(sz)

    sp = np.array(sp.tolist()[:ndims]).astype(float)
    varr = varr / sp
    varr = varr + identity(sz)

    if is_arr_field:
        out = interp_linear_field(
            arr, varr, bg=bg, bg_val=bg_val)
    else:
        out = interp_linear(
            arr, varr, bg=bg, bg_val=bg_val)

    return out


def ApplyVInv(arr, varr, bg=ca.BACKGROUND_STRATEGY_CLAMP,
              bg_val=0.0, sp=ca.Vec3Df(1.0, 1.0, 1.0)):
    out = ApplyV(arr, -varr, bg=bg, bg_val=bg_val, sp=sp)
    return out


def integer_tx(im, off, bg=ca.BACKGROUND_STRATEGY_CLAMP, bg_val=np.inf):

    im = np.squeeze(im)
    sz = np.array(im.shape)
    ndims = len(sz)

    if bg == ca.BACKGROUND_STRATEGY_ZERO or \
            bg == ca.BACKGROUND_STRATEGY_PARTIAL_ZERO:
        bg_val = 0.0

    off = [int(o) for o in off]

    bounds = [[-min(off[dim], 0), max(off[dim], 0)] for dim in range(ndims)]

    if bg == ca.BACKGROUND_STRATEGY_WRAP:
        offIm = im
        for dim in range(ndims):
            offIm = np.roll(offIm, -off[dim], axis=dim)
    elif bg == ca.BACKGROUND_STRATEGY_CLAMP:
        offIm = im
        for dim in range(ndims):
            firstslice = [slice(None) for _ in range(ndims)]
            firstslice[dim] = slice(0, 1)
            firstslice = offIm[tuple(firstslice)]
            lastslice = [slice(None) for _ in range(ndims)]
            lastslice[dim] = slice(sz[dim]-1, sz[dim])
            lastslice = offIm[tuple(lastslice)]
            validslices = [slice(None) for _ in range(ndims)]
            validslices[dim] = slice(bounds[dim][1], sz[dim]-bounds[dim][0])
            validslices = offIm[tuple(validslices)]
            firstslicetile = [1 for _ in range(ndims)]
            firstslicetile[dim] = bounds[dim][0]
            firstslicetile = tuple(firstslicetile)
            lastslicetile = [1 for _ in range(ndims)]
            lastslicetile[dim] = bounds[dim][1]
            lastslicetile = tuple(lastslicetile)
            offIm = np.concatenate(
                (np.tile(firstslice, firstslicetile),
                 validslices,
                 np.tile(lastslice, lastslicetile)),
                axis=dim)
    elif bg in (ca.BACKGROUND_STRATEGY_VAL,
                ca.BACKGROUND_STRATEGY_ZERO,
                ca.BACKGROUND_STRATEGY_PARTIAL_ZERO):
        offIm = np.ones(sz) * bg_val
        validslices = [slice(bounds[dim][1], sz[dim]-bounds[dim][0])
                       for dim in range(ndims)]
        assignslices = [slice(bounds[dim][0], sz[dim]-bounds[dim][1])
                        for dim in range(ndims)]
        offIm[tuple(assignslices)] = im[tuple(validslices)]
    else:
        raise Exception('Unknown boundary condition %s' % repr(bg))

    return offIm


def Translate(arr, off, bg=ca.BACKGROUND_STRATEGY_CLAMP, bg_val=0.0):

    arr = np.squeeze(arr)
    sz = arr.shape
    ndims = len(sz)
    if isinstance(off, ca.Vec3Di) or isinstance(off, ca.Vec3Df):
        off = off.tolist()[:ndims]

    off = np.array(off)

    if issubclass(off.dtype.type, np.integer):
        txarr = integer_tx(arr, off, bg=bg, bg_val=bg_val)
    else:
        off = np.ones(list(arr.shape) + [ndims]) * off
        off = off + identity(sz)
        txarr = interp_linear(
            arr, off, bg=bg, bg_val=bg_val)

    return txarr


def _laplacian(arr,
               bg=ca.BACKGROUND_STRATEGY_CLAMP,
               sp=ca.Vec3Df(1.0, 1.0, 1.0), bg_val=0.0):
    """ image laplacian """
    if isinstance(sp, ca.Vec3Df):
        sp = sp.tolist()
    sp2 = np.array(sp, dtype=np.float)**2
    if isSlice(arr):
        cfac = -2 * (sp2[0] + sp2[1]) / (sp2[0] * sp2[1])
        lap = cfac * arr \
            + Translate(arr, [1, 0], bg=bg, bg_val=bg_val) / sp2[0] \
            + Translate(arr, [-1, 0], bg=bg, bg_val=bg_val) / sp2[0] \
            + Translate(arr, [0, 1], bg=bg, bg_val=bg_val) / sp2[1] \
            + Translate(arr, [0, -1], bg=bg, bg_val=bg_val) / sp2[1]
    else:
        raise Exception('Only 2D arrays currently supported by _laplacian')

    return lap


def Laplacian(arr,
              bg=ca.BACKGROUND_STRATEGY_CLAMP,
              sp=ca.Vec3Df(1.0, 1.0, 1.0), bg_val=0.0):
    """ image or vector field laplacian """
    # handle vector fields as well as images
    if isVectorField(arr):
        arrlist = np.split(arr, arr.shape[-1], axis=-1)
    else:
        arrlist = [arr]

    lap = [_laplacian(np.squeeze(component), bg=bg, sp=sp,
                      bg_val=bg_val) for component in arrlist]
    lap = np.squeeze(np.dstack(lap))
    return lap


if __name__ == '__main__':

    import matplotlib.pyplot as plt
    import PyCA.Common as common

    sz = (64, 62)
    # bg = ca.BACKGROUND_STRATEGY_CLAMP
    # bg = ca.BACKGROUND_STRATEGY_WRAP
    bg = ca.BACKGROUND_STRATEGY_PARTIAL_ZERO

    fsz = (sz[0], sz[1], 2)
    arr = np.random.randn(*sz)

    off = np.array([-5, 3])
    varr = np.ones(fsz) * off

    defarr = ApplyV(arr, varr, bg=bg)
    defarr2 = integer_tx(arr, off, bg=bg)
    plt.figure('test')
    plt.clf()
    plt.subplot(221)
    plt.title('defarr #1')
    plt.imshow(defarr)
    plt.colorbar()
    plt.subplot(222)
    plt.title('defarr #2')
    plt.imshow(defarr2)
    plt.colorbar()
    plt.subplot(223)
    plt.title('orig')
    plt.imshow(arr)
    plt.colorbar()
    plt.subplot(224)
    plt.title('diff')
    plt.imshow(defarr - defarr2)
    plt.colorbar()
    plt.draw()
    plt.show()

    off = np.array([-5.2, 3.8])
    varr = np.ones(fsz) * off
    sp = ca.Vec3Df(0.9, 1.5, 1.0)
    # sp = ca.Vec3Df(1.0, 1.0, 1.0)

    im = common.ImFromNPArr(arr, sp=sp)
    v = common.FieldFromNPArr(varr, sp=sp)
    outim = im.copy()

    defarr = ApplyV(arr, varr, bg=bg, sp=sp)
    ca.ApplyV(outim, im, v, 1.0, bg)
    defimarr = np.squeeze(common.AsNPCopy(outim))

    plt.figure('test2')
    plt.clf()
    plt.subplot(221)
    plt.title('numpy defarr')
    plt.imshow(defarr)
    plt.colorbar()
    plt.subplot(222)
    plt.title('pyca defarr')
    plt.imshow(defimarr)
    plt.colorbar()
    plt.subplot(223)
    plt.title('orig')
    plt.imshow(arr)
    plt.colorbar()
    plt.subplot(224)
    plt.title('diff')
    plt.imshow(defarr - defimarr)
    plt.colorbar()
    plt.draw()
    plt.show()

    harr = VtoH(varr, sp=sp)
    h = v.copy()
    ca.VtoH(h, v)
    defarr = ApplyH(arr, harr, bg=bg)
    ca.ApplyH(outim, im, h, bg)
    defimarr = np.squeeze(common.AsNPCopy(outim))

    plt.figure('ApplyH test')
    plt.clf()
    plt.subplot(221)
    plt.title('numpy defarr')
    plt.imshow(defarr)
    plt.colorbar()
    plt.subplot(222)
    plt.title('pyca defarr')
    plt.imshow(defimarr)
    plt.colorbar()
    plt.subplot(223)
    plt.title('orig')
    plt.imshow(arr)
    plt.colorbar()
    plt.subplot(224)
    plt.title('diff')
    plt.imshow(defarr - defimarr)
    plt.colorbar()
    plt.draw()
    plt.show()

    off = ca.Vec3Di(-3, 2, 0)
    defarr = Translate(arr, off, bg=bg)
    ca.Translate(outim, im, off, bg)
    defimarr = np.squeeze(common.AsNPCopy(outim))

    plt.figure('integer translate test')
    plt.clf()
    plt.subplot(221)
    plt.title('numpy defarr')
    plt.imshow(defarr)
    plt.colorbar()
    plt.subplot(222)
    plt.title('pyca defarr')
    plt.imshow(defimarr)
    plt.colorbar()
    plt.subplot(223)
    plt.title('orig')
    plt.imshow(arr)
    plt.colorbar()
    plt.subplot(224)
    plt.title('diff')
    plt.imshow(defarr - defimarr)
    plt.colorbar()
    plt.draw()
    plt.show()

    off = ca.Vec3Df(-3.3, 2.7, 0)
    defarr = Translate(arr, off, bg=bg)
    ca.Translate(outim, im, off, bg)
    defimarr = np.squeeze(common.AsNPCopy(outim))

    plt.figure('fp translate test')
    plt.clf()
    plt.subplot(221)
    plt.title('numpy defarr')
    plt.imshow(defarr)
    plt.colorbar()
    plt.subplot(222)
    plt.title('pyca defarr')
    plt.imshow(defimarr)
    plt.colorbar()
    plt.subplot(223)
    plt.title('orig')
    plt.imshow(arr)
    plt.colorbar()
    plt.subplot(224)
    plt.title('diff')
    plt.imshow(defarr - defimarr)
    plt.colorbar()
    plt.draw()
    plt.show()
