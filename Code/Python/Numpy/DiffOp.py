import numpy as np
import scipy.sparse.linalg as splinalg
import scipy.sparse as sparse

def SparseLaplacianMat(sz, eps=0.0, as_csr=True, order='F'):
    """
    Compute sparse matrix representing laplacian operator with wrapped
    boundary conditions
    """

    nel = np.prod(sz)
    coords = np.mgrid[:sz[0],:sz[1]]
    c = np.ravel_multi_index((coords[0,:,:], coords[1,:,:]),dims=sz, order=order)
    u = np.ravel_multi_index((coords[0,:,:]-1, coords[1,:,:]),
                             dims=sz, mode='wrap', order=order)
    d = np.ravel_multi_index((coords[0,:,:]+1, coords[1,:,:]),
                             dims=sz, mode='wrap', order=order)
    l = np.ravel_multi_index((coords[0,:,:], coords[1,:,:]-1),
                             dims=sz, mode='wrap', order=order)
    r = np.ravel_multi_index((coords[0,:,:], coords[1,:,:]+1),
                             dims=sz, mode='wrap', order=order)

    row = np.append(c,(c,c,c,c))
    col = np.append(c,(u,d,l,r))
    val = np.append([4.0+eps]*nel,([-1.0]*nel,[-1.0]*nel,[-1.0]*nel,[-1.0]*nel))

    L = sparse.coo_matrix((val,(row,col)),shape=(nel,nel))

    if as_csr:
        L = L.tocsr()
        
    return L

def FourierTx(off, bsz, transpose=False, bilerp=False):
    """
    translation operation in fourier domain
    """

    if bilerp:
        return FourierBilerpTx(off=off, bsz=bsz, transpose=transpose)
    
    off = np.array(off)
    bsz = np.array(bsz)
    ngrid = np.mgrid[-bsz[0]//2:bsz[0]//2,-bsz[1]//2:bsz[1]//2]
    # ngrid = np.mgrid[:bsz[0],:bsz[1]]
    ngrid = ngrid.transpose((1,2,0))/bsz.astype(np.float)
    tx = np.exp(2j*np.pi*np.sum(ngrid*off, axis=2))
    tx = np.roll(np.roll(tx, -bsz[0]//2, axis=0), -bsz[1]//2, axis=1)
    if transpose:
        # for translations, transpose = inverse
        tx = 1.0/tx
        
    return tx

def FourierId(bsz):
    """
    identity operator in fourier domain
    """

    fId = np.ones(bsz, dtype=np.complex)
    return fId
    
def FourierLaplacian(bsz, eps=0.0, lap_pow=1):
    """
    Laplacian operator in fourier domain
    """

    H = (4.0+eps)*FourierId(bsz)
    H -= FourierTx([1, 0], bsz)
    H -= FourierTx([-1, 0], bsz)
    H -= FourierTx([0, 1], bsz)
    H -= FourierTx([0, -1], bsz)

    if lap_pow != 1:
        H = H**lap_pow
        
    return H

