def IOperBG = [
BACKGROUND_STRATEGY_CLAMP,
BACKGROUND_STRATEGY_WRAP,
BACKGROUND_STRATEGY_ZERO,
BACKGROUND_STRATEGY_PARTIAL_ZERO]

template 
void PyCA::Splatting::
splat3D<${IOperBG}>(int*, 
                    size_t, size_t, size_t, 
                    const float*, 
                    const float*, const float*, const float*, 
                    size_t, StreamT);

#if CUDA_ARCH_VERSION >= 20
template 
void PyCA::Splatting::
splat3D<${IOperBG}>(float*, 
                    size_t, size_t, size_t, 
                    const float*, 
                    const float*, const float*, const float*, 
          	    StreamT);
#endif // CUDA_ARCH_VERSION >= 20
