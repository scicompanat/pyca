/* ================================================================
 *
 * PyCA Project
 *
 * Copyright (c) J. Samuel Preston, Linh K. Ha, Sarang C. Joshi. All
 * rights reserved.  See Copyright.txt or for details.
 *
 * This software is distributed WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the above copyright notice for more information.
 *
 * ================================================================ */

#ifndef __CONST_DEVICE_H
#define __CONST_DEVICE_H

#ifndef SWIG
#include <cfloat>
#endif // !SWIG

#define CTA_SIZE              256
#define CUDA_DATA_BLOCK_ALIGN 128
#define BLOCK_ALIGN           128
#define REG_BLOCK_SIZE        256 

#define MAX_NUMBER_DEVICES    256

#define EXEC_CPU       0
#define EXEC_GPU       1
#define EXEC_GPU_PARAM 2

//namespace PyCA {

typedef unsigned int uint;

#define FIX_SCALE_20 1048576.f

// ================================================================
// FFT kernel constant
// ================================================================
// This is now defined and passed in via CMake
// #define MAX_FFT_TABLE_LENGTH 1024

// ================================================================
// enums for interpolation method and background strategy
// ================================================================

#ifndef DEFAULT_INTERP_METHOD
#define DEFAULT_INTERP_METHOD INTERP_LINEAR
#endif // !DEFAULT_INTERP_METHOD

enum InterpT { INTERP_NN, 
	       INTERP_LINEAR, 
	       INTERP_CUBIC};

enum BackgroundStrategy { BACKGROUND_STRATEGY_PARTIAL_ID,
                          BACKGROUND_STRATEGY_ID,
                          BACKGROUND_STRATEGY_PARTIAL_ZERO,
                          BACKGROUND_STRATEGY_ZERO,
                          BACKGROUND_STRATEGY_CLAMP,
                          BACKGROUND_STRATEGY_WRAP,
			  BACKGROUND_STRATEGY_VAL};

// ================================================================
// defines/enums for FiniteDiff 
// ================================================================

#define WRAP_TRUE 1
#define WRAP_FALSE 0
#define ACCUM_TRUE 1
#define ACCUM_FALSE 0
#define SLICE_TRUE 1
#define SLICE_FALSE 0

// ================================================================
// remove need for __host__ and __device__ if not nvcc
// ================================================================

#ifdef __CUDACC__
#define __HOSTDEVICE__ __host__ __device__
#else
#define __HOSTDEVICE__
#endif

namespace PyCA {

const float MIN_VAL = FLT_MIN;
const float MAX_VAL = FLT_MAX;

// template params for specifying direction 
enum DimT {DIM_X, DIM_Y, DIM_Z};
// finite difference types
enum DiffT {DIFF_FORWARD, DIFF_BACKWARD, DIFF_CENTRAL};
// Specify the action of derivative operators at image boundary.
// Wrapping takes differences wrt wrapped coordinates, while clamping
// clamps the value at the edge, essentially enforcing zero derivative
// for forward or backward differences (though not central).  The
// difference between BC_APPROX and BC_CLAMP is only meaningful for
// central differences -- BC_APPROX takes the appropriate forward or
// backward difference at the boundary to approximate the central
// difference instead of standard clamping behavior.  This makes the
// derivative field seem continuous, not dropping to zero at one
// boundary for forward or backward differences, or having an
// approximately half-valued derivative at all boundaries for central
// differences. The original AtlasWerks code used the approximation
// boundary conditions, so this is the default.  As there can be
// confusion between the BackgroundStrategy enum and the
// BoundaryCondition enum, wrapping and clamping values for each are
// set to match.
enum BoundaryCondT {BC_WRAP=BACKGROUND_STRATEGY_WRAP, 
		    BC_CLAMP=BACKGROUND_STRATEGY_CLAMP, 
		    BC_APPROX=10};
// operation to perform before returning value.  OP_VAL returns the
// value (no additional operation) OP_SQR returns the square of the
// calculated value.
enum OpT{OP_VAL,OP_SQR};

} // end namespace PyCA

#endif
